<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class NoteController extends BaseController
{
    public function home(Request $request){
        if ($request->isMethod('post')) {
            $noteId = $request->input('delete_note');
            DB::table('test1')->where('note_id', $noteId)->delete();
            return redirect('/');
        }
        $data = DB::table('test1')
                    ->orderBy('note_id', 'DESC')
                    ->paginate(3);
        return view('home', compact('data'));
    }
    public function create(Request $request){
        if ($request->isMethod('post')) {
            $note_name = $request->input('note_name');
            $note_text = $request->input('note_text');
            $note_picture = $request->file('note_picture')->store('uploads', 'public');
            DB::table('test1')->insert([
                'note_name' => $note_name,
                'note_text' => $note_text,
                'note_picture'=>$note_picture,
            ]);
            //return redirect('/');
        }
        return view('create');

    }

}
