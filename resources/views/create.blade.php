@extends('layouts.panel')

@section('content')
    <form action="#" method="post" enctype="multipart/form-data">
        @csrf
        <div>
            <label for="">Enter Title</label>
            <input type="text" name="note_name">
        </div>
        <div>
            <label for="">Enter Text</label>
            <textarea name="note_text" rows="8" cols="80"></textarea>
        </div>
        <div>
            <label for="">Load Picture</label>
            <input type="file" name="note_picture">
        </div>
        <br><br><br>
        <div>
            <input type="submit" name="note_create" value="Create">
        </div>
    </form>
@endsection
