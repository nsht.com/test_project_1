@extends('layouts.panel')

@section('content')
    @foreach ($data as $item)
        @isset ($item->note_picture)
        <img width="200px" height="100%" src="{{ asset('/storage/' . $item->note_picture) }}">
        @endisset
        <h2>{{ $item->note_name }}</h2>
        <p>{{ $item->note_text }}</p>
        <form onsubmit="return confirm('Do you really want to delete the note?');" action="#" method="post">
            @csrf
            <button type="submit" name="delete_note" value="{{ $item->note_id }}">Delete</button>
        </form>
        <hr>
    @endforeach
    {{ $data->links() }}
@endsection
