<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Note</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->

    </head>
    <style>
      .pagination{
         display: flex;
         list-style-type: none;
      }
    </style>
    <body>
        <div class="links">
            <a href="/">home</a>
            <a href="/create/">create</a>
            <hr>
            @yield('content')
        </div>
    </body>
</html>
